<?php include_once '../views/layout/header_view.php'; ?>

<!-- Main Section -->
	<section class="row main-content">
		<div class="col-md-12">
			<div class="user-content">
				<h1>Students:</h1>
				<a href="<?php echo $config['base_url']?>student/create.php" class="btn btn-success">ADD NEW</a>
				<hr>
				
				<table class="table table-bordered">
					<tr>
						<th>SL.</th>
						<th>ID</th>
						<th>Name</th>
						<th>Username</th>
						<th>Email</th>
						<th>Password</th>
						<th>Contact</th>
						<th>File</th>
						<th>Action</th>
					</tr>
					<?php 
							if (!empty($data_student)) {
								
							
							
								// Fetch data to show in table

								$i = 0;

								foreach ($data_student as $data) { 
									$i++;
								?>
									
								<tr>
									<td> <?php echo $i; ?> </td>
									<td> <?php echo $data['id']; ?> </td>
									<td> <?php echo $data['name']; ?> </td>
									<td> <?php echo $data['username']; ?> </td>
									<td> <?php echo $data['email']; ?> </td>
									<td> <?php echo $data['password']; ?> </td>
									<td> <?php echo $data['contact']; ?> </td>
									<!-- <td> <?php echo $data['file']; ?> </td> -->
									<td> 
										<?php $src = (!empty($data['file']) ? '../assets/file_manager/'.$data['file'] : '../assets/file_manager/user.png') ?>

										<img src="<?php echo $src;?>" width="60px" height="60px"> 
									</td>
									<td>
										<?php echo "<a href='edit.php?action=edit&id=".$data['id']."'>Edit</a>" ?>
					 						||
										<?php echo "<a href='delete.php?action=delete&id=".$data['id']."' onClick='return confirm(\"Are You sure to Delete Data ...\")'>Delete</a>" ?>
									</td>
								</tr>

							<?php } 

							} 
							else {
							?>
							<tr>
								<td colspan="8" class="text-center">No Records Found!</td>
							</tr>
						<?php	
						}
					?>
				</table>

			</div>
		</div>
	</section>

<?php include_once '../views/layout/footer_view.php'; ?>