<?php include_once '../views/layout/header_view.php'; ?>

<!-- Main Section -->
	<section class="row main-content">
		<div class="col-md-12">
			<div class="usertype-content">
				<h1>Usertypes:</h1>
				<a href="<?php echo $config['base_url']?>usertype/index.php" class="btn btn-success">Usertype List</a>
				<hr>
				
				<form class="form-horizontal" method="POST" action="">
				  	
				  	<?php
						// Show Success/Error message
						
						if (isset($msg)) {
					?>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-offset-1 col-sm-6">

								<div class="alert alert-success">
									<?php echo (isset($msg)) ? $msg : ''; ?>
								</div>
							</div>
						</div>
					<?php
						}
					?>



				  	<div class="form-group">
				    	<label for="usertype" class="col-sm-4 control-label">User Type</label>
					    <div class="col-sm-4">
					      <input type="text" class="form-control" id="usertype" name="usertype" placeholder="Enter User Type">
					    </div>
				  	</div>
				 
				  	<div class="form-group">
				    	<div class="col-sm-offset-4 col-sm-10">
				      		<button type="submit" name="create" class="btn btn-success">Submit</button>
				    	</div>
				  	</div>
				</form>

			</div>
		</div>
	</section>

<?php include_once '../views/layout/footer_view.php'; ?>