<?php include_once '../views/layout/header_view.php'; ?>

<!-- Main Section -->
	<section class="row main-content">
		<div class="col-md-12">
			<div class="usertype-content">
				<h1>Usertypes:</h1>
				<a href="<?php echo $config['base_url']?>usertype/create.php" class="btn btn-success">ADD NEW</a>
				<hr>
				
				<table class="table table-bordered">
					<tr>
						<th>SL.</th>
						<th>ID</th>
						<th>User Type</th>
						<th>Action</th>
					</tr>
					<?php 

								// Fetch data to show in table

								$i = 0;

								foreach ($data_usertype as $data) { 
									$i++;
							?>
									
								<tr>
									<td> <?php echo $i; ?> </td>
									<td> <?php echo $data['id']; ?> </td>
									<td> <?php echo $data['user_type']; ?> </td>
									
									<td>
										<?php echo "<a href='edit.php?action=edit&id=".$data['id']."'>Edit</a>" ?>
					 						||
										<?php echo "<a href='delete.php?action=delete&id=".$data['id']."' onClick='return confirm(\"Are You sure to Delete Data ...\")'>Delete</a>" ?>
									</td>
								</tr>

							<?php } ?>
					
				</table>

			</div>
		</div>
	</section>

<?php include_once '../views/layout/footer_view.php'; ?>