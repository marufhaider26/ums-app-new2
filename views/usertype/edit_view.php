<?php include_once '../views/layout/header_view.php'; ?>

	<section class="main-content">
		<div class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a href="<?php echo $config['base_url']; ?>usertype/index.php" class="btn btn-info">+ usertype List</a>
						<h3>usertype Entry</h3>

						<form action="" method="POST" class="form-horizontal">
							
							<?php
								// Show Success/Error message
							
								if (isset($msg)) {
							?>
								<div class="form-group">
									<label for="" class="col-sm-2 control-label">&nbsp;</label>
									<div class="col-sm-offset-1 col-sm-6">

										<div class="alert alert-success">
											<?php echo (isset($msg)) ? $msg : ''; ?>
										</div>
									</div>
								</div>
							<?php
								}
							?>
							

							<div class="form-group">
								<label for="usertype" class="col-sm-2 control-label">User Type</label>
								<div class="col-sm-offset-1 col-sm-6">
									<input type="hidden" class="form-control" id="id" name="id" value="<?php echo $data['id']; ?>">
									<input type="text" class="form-control" id="usertype" name="usertype" value="<?php echo $data['user_type']; ?>" required>
								</div>
							</div>


							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-6">
									<button type="submit" name="edit" class="btn btn-success">Update</button>
								</div>
							</div>
						</form>

						
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include_once '../views/layout/footer_view.php'; ?>