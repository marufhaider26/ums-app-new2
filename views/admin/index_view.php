<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<title> AIA - Login </title>

	<!-- CSS File -->
	<link rel="stylesheet" type="text/css" href="<?php echo $config['base_url']; ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config['base_url']; ?>assets/css/style.css">
</head>
<body>

	<header>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-center">Welcome to UMS Admin Panel</h1>
				</div>
			</div>
		</div>
	</header>

	<!-- Login Panel -->
	<section class="main-content">
		<div class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="bg-info login-panel" >
							<h3 class="text-center">UMS - Login Panel</h3>
							<form action="" method="POST" class="form-horizontal">
								
								<?php
									// Show Success/Error message
									if (isset($msg)) {
								?>
									<div class="form-group">
										<label for="name" class="col-sm-2 control-label">&nbsp;</label>
										<div class="col-sm-8">

											<div class="alert alert-danger">
												<span style="color: #DE5A24; font-weight: bold;"> 
													<?php echo (isset($msg)) ? $msg : ''; ?>
												</sapn>
											</div>
										</div>
									</div>
								<?php
									}
								?>

								<div class="form-group">
									<label for="username" class="col-sm-offset-2 col-sm-2 control-label">Username</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="username" name="username" placeholder="Username">
									</div>
								</div>
								<div class="form-group">
									<label for="password" class="col-sm-offset-2 col-sm-2 control-label">Password</label>
									<div class="col-sm-6">
										<input type="password" class="form-control" id="password" name="password" placeholder="Password">
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-offset-7 col-sm-6">
										<button type="submit" name="login" class="btn btn-success">Log in</button>
									</div>
								</div>
							</form>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
	</section>

<?php include_once '../views/layout/footer_view.php'; ?>