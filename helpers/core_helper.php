<?php
	
	/**
	 * Dump data for helpeer function
	 *
	 * @param       mixed  $data    Input string
	 * @param       bool  $die    Input string
	 * @return      mixed
	 */
	function dump($data, $die=FALSE)
	{
		echo '<pre>';
		print_r($data);
		echo '</pre>';

		if ($die) {
			die('Dumped Here!!!');
		}

	}


		/**
		 * Redirect URL
		 *
		 * @param       string  $url    Input URL
		 * @return      string
		 */

		function redirect($url) {
	    	return header('Location: '.$url);
	    }