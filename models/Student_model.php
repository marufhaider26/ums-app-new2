
<?php

	// Load core required file
	require_once '../library/DB.php';
	/**
	* 
	*/
	class Student_model extends DB 
	{
		protected $table = 'tbl_student';

		private $name;
		private $email;
		private $username;
		private $password;
		private $uid;
		private $contact;
		private $file;



		
		// Set Name
		public function setName($name) {
			$this->name = $name;
		}

		// Set Email
		public function setEmail($email) {
			$this->email = $email;
		}

		// Set Username
		public function setUsername($username) {
			$this->username = $username;
		}

		// Set Password
		public function setPassword($password) {
			$this->password = $password;
		}
		
		// Set Contact
		public function setContact($contact) {
			$this->contact = $contact;
		}

		public function setUid($utype) {
			$this->uid = $utype;
		}
		public function setFile($file) {
			$this->file = $file;
		}


		// Save New User 
		public function save_student() {

			$query = "INSERT INTO tbl_users(username, password, user_type_id) VALUES ('$this->username', '$this->password', '$this->uid')";
			$user_id  = $this->insert($query);
			
			// dump($user_id, TRUE);
			$query = "INSERT INTO $this->table(name, email, contact, user_id, file) VALUES ('$this->name', '$this->email', '$this->contact', '$user_id', '$this->file')";
			 
			return $this->insert($query);
		}

		// Fetch User Data 
		public function get_students()
		{
			$sql = "
						SELECT tbl_student.*, tbl_users.username , tbl_users.password, tbl_user_type.user_type 
						FROM tbl_student
						LEFT JOIN tbl_users
						ON tbl_student.user_id = tbl_users.id
						LEFT JOIN tbl_user_type
						ON tbl_users.user_type_id = tbl_user_type.id
					";
			return $this->custom_query($sql);
			// return $this->select_all($this->table);
		}


		// Fetch student Data by ID
		public function get_student_by_id($id)
		{
			$sql = "
						SELECT tbl_student.*, tbl_users.username , tbl_users.password, tbl_user_type.user_type 
						FROM tbl_student
						LEFT JOIN tbl_users
						ON tbl_student.user_id = tbl_users.id
						LEFT JOIN tbl_user_type
						ON tbl_users.user_type_id = tbl_user_type.id WHERE tbl_student.id = '$id'
					";

			// dump($sql);
			return $this->custom_query_array($sql);

			// return $this->select_by_id($id, $this->table);
		}

		// Update student
		public function update_student($id) {

			$query = "UPDATE $this->table SET name='$this->name', email='$this->email', contact='$this->contact', file='$this->file'  WHERE id='$id'";

			
			if ($this->update($query)) {

				$data_stu = $this->get_student_by_id($id);
				$user_id = $data_stu['user_id'];
				
				$query = "UPDATE tbl_users SET username='$this->username', password='$this->password', user_type_id='$this->uid' WHERE id='$user_id'";

					return $this->update($query);
			}
			
		}

		// Delete User by ID
		public function del_student($id)
		{
			$data_stu = $this->get_student_by_id($id);
			$user_id = $data_stu['user_id'];

			if ($this->delete($id, $this->table)) {

				return $this->delete($user_id, 'tbl_users');
			}
		}

		// Check valid User or Not
		public function validate_user_login($username, $password) {
			$data = $this->select_by_un_pass($username, $password);
			// dump($data, TRUE);
			return ($username === $data['username'] && $password === $data['password']);
		}
	}