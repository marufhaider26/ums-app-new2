
<?php

	// Load core required file
	require_once '../library/DB.php';
	/**
	* 
	*/
	class Usertype_model extends DB 
	{
		protected $table = 'tbl_user_type';

		public $usertype;

		
		// Set Name
		public function setUsertype($utype) {
			$this->usertype = $utype;
		}


		// Save New usertype 
		public function save_usertype() {

			$query = "INSERT INTO $this->table(user_type) VALUES( '$this->usertype')";
			// dump($query, TRUE);
			return $this->insert($query);
		}

		// Fetch usertype Data 
		public function get_usertype()
		{
			return $this->select_all($this->table);
		}


		// Fetch usertype Data by ID
		public function get_usertype_by_id($id)
		{
			return $this->select_by_id($id, $this->table);
		}

		// Update usertype
		public function update_usertype($id) {

			$query = "UPDATE $this->table SET user_type='$this->usertype' WHERE id='$id'";

			return $this->update($query);
		}

		// Delete usertype by ID
		public function del_usertype($id)
		{
			return $this->delete($id, $this->table);
		}

		
	}