
<?php

	// Load core required file
	require_once '../library/DB.php';
	/**
	* 
	*/
	class User_model extends DB
	{
		protected $table = 'tbl_users';

		private $name;
		private $email;
		private $username;
		private $password;
		private $contact;
		private $usertype;
		private $file = '';
		


		function __construct()
		{
			// $this->link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			parent::__construct();
		}

		// Set Name
		public function setName($name) {
			$this->name = $name;
		}

		// Set Email
		public function setEmail($email) {
			$this->email = $email;
		}

		// Set Username
		public function setUsername($username) {
			$this->username = $username;
		}

		// Set Password
		public function setPassword($password) {
			$this->password = $password;
		}
		// Set Password
		public function setUserType($utype) {
			$this->usertype = $utype;
		}
		
		// Set Contact
		public function setContact($contact) {
			$this->contact = $contact;
		}


		// Save New User 
		public function save_user() {

			$query = "INSERT INTO $this->table(name, email, username, password, uid, contact, file) VALUES( '$this->name', '$this->email', '$this->username', '$this->password', $this->usertype, '$this->contact', '$this->file')";
			// dump($query, TRUE);
			$insert_id =  $this->insert($query);
			$query = "INSERT INTO $this->table(name, email, username, password, uid, contact, file) VALUES( '$this->name', '$this->email', '$this->username', '$this->password', $this->usertype, '$this->contact', '$this->file')";
			// dump($query, TRUE);
			$insert_id =  $this->insert($query);
			dump($insert_id, TRUE);
		}

		// Fetch User Data 
		public function get_users()
		{
			$sql = "
						SELECT tbl_users.*, tbl_user_type.user_type
						FROM tbl_users
						LEFT JOIN tbl_user_type
						ON tbl_users.uid = tbl_user_type.id
					";
			return $this->custom_query($sql);
		}


		// Fetch User Data by ID
		public function get_user_by_id($id)
		{
			$sql = "SELECT tbl_users.*, tbl_user_type.user_type 
					FROM `tbl_users`
					LEFT JOIN tbl_user_type
					ON tbl_users.uid = tbl_user_type.id
					WHERE tbl_users.id = '$id'";
			return $this->custom_query_array($sql);
		}

		// Update User
		public function update_user($id) {

			$query = "UPDATE $this->table SET name='$this->name', email='$this->email', username='$this->username', password='$this->password', uid = '$this->usertype', contact='$this->contact' WHERE id='$id'";
			// dump($query, TRUE);
			return $this->update($query);
		}

		// Delete User by ID
		public function del_user($id)
		{
			return $this->delete($id, $this->table);
		}

		// Check valid User or Not
		public function validate_user_login($username, $password) {
			$data = $this->select_by_un_pass($username, $password);
			// dump($data, TRUE);
			return ($username === $data['username'] && $password === $data['password']);
		}
	}