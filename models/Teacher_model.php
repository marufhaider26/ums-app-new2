
<?php

	// Load core required file
	require_once '../library/DB.php';
	/**
	* 
	*/
	class Teacher_model extends DB 
	{
		protected $table = 'tbl_teacher';

		private $name;
		private $email;
		private $password;
		private $contact;

		
		// Set Name
		public function setName($name) {
			$this->name = $name;
		}

		// Set Email
		public function setEmail($email) {
			$this->email = $email;
		}

		// Set Password
		public function setPassword($password) {
			$this->password = $password;
		}
		
		// Set Contact
		public function setContact($contact) {
			$this->contact = $contact;
		}


		// Save New User 
		public function save_teacher() {

			$query = "INSERT INTO $this->table(name, email, password, contact) VALUES ('$this->name', '$this->email', '$this->password', '$this->contact')";
			 //dump($query, TRUE);
			return $this->insert($query);
		}

		// Fetch User Data 
		public function get_teachers()
		{
		
			return $this->select_all($this->table);
		}


		// Fetch student Data by ID
		public function get_teacher_by_id($id)
		{
			return $this->select_by_id($id, $this->table);
		}

		// Update student
		public function update_teacher($id) {

			$query = "UPDATE $this->table SET name='$this->name', email='$this->email', password='$this->password', contact='$this->contact' WHERE id='$id'";

			return $this->update($query);
		}

		// Delete User by ID
		public function del_teacher($id)
		{
			return $this->delete($id, $this->table);
		}

		// Check valid User or Not
		public function validate_user_login($username, $password) {
			$data = $this->select_by_un_pass($username, $password);
			// dump($data, TRUE);
			return ($username === $data['username'] && $password === $data['password']);
		}
	}