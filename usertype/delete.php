<?php


	// usertypetype Create Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';
	require_once '../models/Usertype_model.php';

	// Delete usertype
	$usertype = new Usertype_model(); 

	if(isset($_GET['action']) && $_GET['action']=='delete') {
		$id = (int)$_GET['id'];

		if($usertype->del_usertype($id)) {

			$msg = 'Data Deleted Successfully!';
			$url = $config['base_url'].'usertype/index.php?msg='.urlencode($msg);

			redirect($url);
		}
		else {
			$msg = 'Something Wrong Here!!!';
		}

	}