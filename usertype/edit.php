<?php
	// usertype Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';

	require_once '../models/Usertype_model.php';

	// Fetch usertype data
	$usertype = new Usertype_model(); 
	if(isset($_GET['action']) && $_GET['action']=='edit') {
		$id = (int)$_GET['id'];
		$data = $usertype->get_usertype_by_id($id);

		dump($data);
	}

	// Update usertype data
	if (isset($_POST['edit'])) {

		$id 		= $_GET['id'];
		// $id 		= $_POST['id'];
		$utype 		= $_POST['usertype'];
		

		$usertype->setUsertype($utype);
		

		if ($usertype->update_usertype($id)) {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Data Updated Successfully! </sapn>';
		}
		else {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Something Wrong Here!!! </sapn>';
		}
	}

	// View File
	require '../views/usertype/edit_view.php';