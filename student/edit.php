<?php
	// User Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';

	require_once '../models/Student_model.php';
	require_once '../models/Usertype_model.php';


	// Fetch usertype data
	$usertype = new Usertype_model(); 
	$data_usertype = $usertype->get_usertype();
	
	// Fetch User data
	$student = new Student_model(); 
	if(isset($_GET['action']) && $_GET['action']=='edit') {
		$id = (int)$_GET['id'];
		$data = $student->get_student_by_id($id);

		// dump($data);
	}

	// Update User data
	if (isset($_POST['edit'])) {

		$id 		= $_GET['id'];
		// $id 		= $_POST['id'];
		$name 		= $_POST['name'];
		$username 	= $_POST['username'];
		$email 		= $_POST['email'];
		$password 	= $_POST['password'];
		$contact 	= $_POST['contact'];
		$utype   	= $_POST['utype'];

		$filename 	= $_FILES['file']['name'];
		$filesize   = $_FILES['file']['size'];
		$filetmp    = $_FILES['file']['tmp_name'];
		$filetype   = $_FILES['file']['type'];
		$fileext    = strtolower(end(explode('.',$_FILES['file']['name'])));

		$expensions = array("jpeg","jpg","png");

		if ( in_array($fileext,$expensions) === false ) {
			$fileErr = "Extension not allowed, Please Choose a JPEG or PNG file.";
		}

		if ( $filesize > 2097152 ) {
			$fileErr        = 'File size must be excately 2 MB';
		}

		if ( empty($fileErr) == true ) {

			$updir = '../assets/file_manager/';
			$filepath       = $updir.$filename;

			// dumper($filename, TRUE);

			move_uploaded_file($filetmp,$filepath);
		}
		

		$student->setName($name);
		$student->setUsername($username);
		$student->setEmail($email);
		$student->setPassword($password);
		$student->setContact($contact);
		$student->setUid($utype);
		$student->setFile($filename);


		if ($student->update_student($id)) {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Data Updated Successfully! </sapn>';
		}
		else {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Something Wrong Here!!! </sapn>';
		}
	}

	// View File
	require '../views/student/edit_view.php';