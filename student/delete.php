<?php


	// User Create Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';
	require_once '../models/Student_model.php';

	// Delete user
	$student = new Student_model(); 

	if(isset($_GET['action']) && $_GET['action']=='delete') {
		$id = (int)$_GET['id'];

		if($student->del_student($id)) {

			$msg = 'Data Deleted Successfully!';
			$url = $config['base_url'].'student/index.php?msg='.urlencode($msg);

			redirect($url);
		}
		else {
			$msg = 'Something Wrong Here!!!';
		}

	}