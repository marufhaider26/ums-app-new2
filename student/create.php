<?php
	// User Create Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';
	require_once '../models/Student_model.php';
	require_once '../models/Usertype_model.php';


	// Fetch usertype data
	$usertype = new Usertype_model(); 
	$data_usertype = $usertype->get_usertype();

	// Fetch usertype data
	//$student = new Student_model(); 
	//$data_student = $student->get_student();
	// dump($data_usertype);

	$student = new Student_model();

	if (isset($_POST['create'])) {

		// dump($_POST);
		// dump($_FILES, TRUE);
		$name = $_POST['name'];
		$username = $_POST['username'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$contact = $_POST['contact'];
		$utype = $_POST['utype'];

		$filename 	= $_FILES['file']['name'];
		$filesize   = $_FILES['file']['size'];
		$filetmp    = $_FILES['file']['tmp_name'];
		$filetype   = $_FILES['file']['type'];
		$fileext    = strtolower(end(explode('.',$_FILES['file']['name'])));

		$expensions = array("jpeg","jpg","png");

		if ( in_array($fileext,$expensions) === false ) {
			$fileErr = "Extension not allowed, Please Choose a JPEG or PNG file.";
		}

		if ( $filesize > 2097152 ) {
			$fileErr        = 'File size must be excately 2 MB';
		}

		if ( empty($fileErr) == true ) {

			$updir = '../assets/file_manager/';
			$filepath       = $updir.$filename;

			// dumper($filename, TRUE);

			move_uploaded_file($filetmp,$filepath);
		}
	

		$student->setName($name);
		$student->setUsername($username);
		$student->setEmail($email);
		$student->setPassword($password);
		$student->setContact($contact);
		$student->setUid($utype);
		$student->setFile($filename);
	

		if ($student->save_student()) {

			$msg = '<span style="color: #06960E; font-weight: bold;"> Data Inserted Successfully! </sapn>';

			// $url = "index.php";
			// redirect($url);
		}
		else {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Something Wrong Here!!! </sapn>';
		}

	}

	// Load User create view file
	include_once '../views/student/create_view.php';
?>



		

		