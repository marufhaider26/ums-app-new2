<?php 
	require_once '../config/db_config.php';
	require_once '../helpers/core_helper.php';
	/**
	 * DB Class for manipulating data in database.
	 *
	 * @category    Libraries
	 * @author      Shaquib Mahmud
	 * @link        http://www.sqbctgbd.com
	 */
	class DB
	{
		public $link;
		public $error;

		function __construct()
		{
			$this->link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		}

		/**
		 * Database connection
		 *
		 * @return 		obj
		 */
		
		// public function connection() {

		// 	if (!isset($this->link)) {
		// 		return new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
				
		// 	}

		// 	if($this->link === FALSE) {
		// 		$this->error = "Connection fail" . $this->link->connect_error;
		// 		return false;
		// 	}
		// }

		/**
		 * SELECT ALL DATA
		 *
		 * @param       string  $table    Input table name
		 * @return 		array
		 */

		public function select_all($table) {
			
			$query = "SELECT * FROM $table";

			$result = $this->link->query($query);

			if ($result->num_rows > 0) {
				while($row = $result -> fetch_assoc()) {
					$records[] = $row;
				}

				$result -> free();
				return $records;
			}
			else {
				return false;
			}
		}

		/**
		 * Select Data BY ID
		 *
		 * @param       number  $id 	  Input ID
		 * @param       string  $table    Input table name
		 * @return 		array
		 */

		public function select_by_id($id, $table) {

			$query = "SELECT * FROM $table WHERE id=$id";

			$result = $this->link->query($query);
			
			if ($result->num_rows > 0) {
				return $result->fetch_assoc();
			}
			else {
				return false;
			}
		}

		/**
		 * Select Data BY Username and Password
		 *
		 * @param       string  $username 	  Input Username
		 * @param       string  $password 	  Input Password
		 * @return 		array
		 */

		public function select_by_un_pass($username, $password) {

			$query = "SELECT * FROM tbl_users WHERE username='$username' AND password='$password'";
			// dump($query, TRUE);
			$result = $this->link->query($query);
			
			if ($result->num_rows > 0) {
				return $result->fetch_assoc();
			}
			else {
				return false;
			}
		}

		/**
		 * Insert Data
		 *
		 * @param       string  $query 	  Input Query
		 * @return 		bool
		 */

		public function insert($query) {

			// $insert_row = $this->link->query($query);
			if ($this->link->query($query) == TRUE) {
    			$last_insert_id = $this->link->insert_id;
    		}
			// if ($insert_row) {
			//     $last_id = $this->link->insert_id;
			// }
    		// dump($last_insert_id, TRUE);
			if (!$last_insert_id) {
				die("Error :(".$this->link->errno.")".$this->link->error);
			}

			return $last_insert_id;
		}

		// public static function last_insert_id($query) {

		// 	$last_id = $this->link->insert_id;

		// 	return $last_id;
		// }

		/**
		 * Update Data
		 *
		 * @param       string  $query 	  Input Query
		 * @return 		bool
		 */

		public function update($query) {

			$update_row = $this->link->query($query);

			if (!$update_row) {
				die("Error :(".$this->link->errno.")".$this->link->error);
			}

			return $update_row;
		}


		/**
		 * Delete Data
		 *
		 * @param       number  $id 	  Input ID
		 * @param       string  $table    Input table name
		 * @return 		bool
		 */

		public function delete($id, $table) {

			$query = "DELETE FROM $table WHERE id=$id";

			$delete_row = $this->link->query($query);

			if (!$delete_row) {
				die("Error :(".$this->link->errno.")".$this->link->error);
			}

			return $delete_row;
		}


		/**
		 * Escape Data
		 *
		 * @param       string  $input    Input data
		 * @return 		bool
		 */
		public function escape($input) {
			$str = $this->link->real_escape_string($input);
			$escapeStr = htmlspecialchars(htmlentities($str));
			return $escapeStr;
		}


		/**
		 * SELECT ALL DATA from Multiple Table (Joining)
		 *
		 * @param       string  $table    Input table name
		 * @return 		array
		 */

		public function custom_query($query) {
			
			// $query = "SELECT * FROM $table";

			$result = $this->link->query($query);

			if ($result->num_rows > 0) {
				while($row = $result -> fetch_assoc()) {
					$records[] = $row;
				}

				$result -> free();
				return $records;
			}
			else {
				return false;
			}
		}


		public function custom_query_array($query) {

			// $query = "SELECT * FROM $table WHERE id=$id";

			$result = $this->link->query($query);
			
			if ($result->num_rows > 0) {
				return $result->fetch_assoc();
			}
			else {
				return false;
			}
		}

		// Check LoggedIn or Not
		public function is_logged_in() {

			if ($_SESSION['username'] == NULL &&  $_SESSION['logged_in']!= TRUE) {

				return false;
			}
			else {
				return true;
			}
		}

	}