<?php
	// User Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';

	require_once '../models/Teacher_model.php';

	// Fetch User data
	$teacher = new Teacher_model(); 
	if(isset($_GET['action']) && $_GET['action']=='edit') {
		$id = (int)$_GET['id'];
		$data = $teacher->get_teacher_by_id($id);

		// dump($data);
	}

	// Update User data
	if (isset($_POST['edit'])) {

		$id 		= $_GET['id'];
		// $id 		= $_POST['id'];
		$name 		= $_POST['name'];
		$email 		= $_POST['email'];
		$password 	= $_POST['password'];
		$contact 	= $_POST['contact'];


		$teacher->setName($name);
		$teacher->setEmail($email);
		$teacher->setPassword($password);
		$teacher->setContact($contact);


		if ($teacher->update_teacher($id)) {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Data Updated Successfully! </sapn>';
		}
		else {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Something Wrong Here!!! </sapn>';
		}
	}

	// View File
	require '../views/teacher/edit_view.php';