<?php


	// User Create Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';
	require_once '../models/Teacher_model.php';

	// Delete user
	$teacher = new Teacher_model(); 

	if(isset($_GET['action']) && $_GET['action']=='delete') {
		$id = (int)$_GET['id'];

		if($teacher->del_teacher($id)) {

			$msg = 'Data Deleted Successfully!';
			$url = $config['base_url'].'teacher/index.php?msg='.urlencode($msg);

			redirect($url);
		}
		else {
			$msg = 'Something Wrong Here!!!';
		}

	}