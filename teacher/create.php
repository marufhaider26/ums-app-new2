<?php
	// User Create Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';
	require_once '../models/Teacher_model.php';


	// Fetch usertype data
	//$student = new Student_model(); 
	//$data_student = $student->get_student();
	// dump($data_usertype);

	$teacher = new Teacher_model();

	if (isset($_POST['create'])) {

		// dump($_POST, TRUE);
		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$contact = $_POST['contact'];
	

		$teacher->setName($name);
		$teacher->setEmail($email);
		$teacher->setPassword($password);
		$teacher->setContact($contact);
	

		if ($teacher->save_teacher()) {

			$msg = '<span style="color: #06960E; font-weight: bold;"> Data Inserted Successfully! </sapn>';

			// $url = "index.php";
			// redirect($url);
		}
		else {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Something Wrong Here!!! </sapn>';
		}

	}

	// Load User create view file
	include_once '../views/teacher/create_view.php';
?>



		

		